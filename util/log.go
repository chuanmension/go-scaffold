package util

import (
	"log"
	"os"
	"path/filepath"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

// InitLog in global
func InitLog(debug bool, logFile string) *zap.Logger {
	var loggerConfig zap.Config
	if debug {
		// dev
		loggerConfig = zap.NewDevelopmentConfig()
	} else {
		logPath := filepath.Dir(logFile)
		if !CheckPathExist(logPath) {
			println("InitLog create log path:", logPath)
			os.MkdirAll(logPath, 0755)
		}
		if !CheckPathExist(logFile) {
			os.Create(logFile)
		}
		// prod
		loggerConfig = zap.NewProductionConfig()
		loggerConfig.OutputPaths = []string{logFile}
		loggerConfig.ErrorOutputPaths = []string{logFile}
		loggerConfig.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	}
	logger, err := loggerConfig.Build()
	if err != nil {
		log.Fatal(err)
	}
	defer logger.Sync()
	zap.ReplaceGlobals(logger)
	// defer undo()
	return logger
}

// InitRotateLog export
func InitRotateLog(debug bool, logFile string) *zap.Logger {
	var loggerConfig zapcore.EncoderConfig
	if debug {
		return InitLog(debug, logFile)
	}
	logPath := filepath.Dir(logFile)
	if !CheckPathExist(logPath) {
		println("InitLog create log path:", logPath)
		os.MkdirAll(logPath, 0755)
	}
	if !CheckPathExist(logFile) {
		os.Create(logFile)
	}
	loggerConfig = zap.NewProductionEncoderConfig()
	loggerConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	// init rotate hook
	hook := &lumberjack.Logger{
		Filename:   logFile,
		MaxSize:    10, // megabytes
		MaxBackups: 10,
		MaxAge:     365,
		Compress:   false,
	}
	defer hook.Close()
	w := zapcore.AddSync(hook)
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(loggerConfig),
		w,
		zap.InfoLevel,
	)
	logger := zap.New(core)
	zap.ReplaceGlobals(logger)
	return logger
}
