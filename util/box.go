package util

// IndexOfInt32 export
func IndexOfInt32(s []int32, d int32) int {
	for i, v := range s {
		if v == d {
			return i
		}
	}
	return -1
}

// MinInt for int
func MinInt(x, y int) int {
	if x < y {
		return x
	}
	return y
}

// MaxInt for int
func MaxInt(x, y int) int {
	if x > y {
		return x
	}
	return y
}

// MinInt32 for int32
func MinInt32(x, y int32) int32 {
	if x < y {
		return x
	}
	return y
}

// MaxInt32 for int32
func MaxInt32(x, y int32) int32 {
	if x > y {
		return x
	}
	return y
}

// Min for int64
func Min(x, y int64) int64 {
	if x < y {
		return x
	}
	return y
}

// Max for int64
func Max(x, y int64) int64 {
	if x > y {
		return x
	}
	return y
}

// AbsInt64 for int64
func AbsInt64(n int64) int64 {
	y := n >> 63
	return (n ^ y) - y
}

// ConcatBytes TL;DR
func ConcatBytes(arr ...[]byte) []byte {
	var size int
	for _, v := range arr {
		size += len(v)
	}
	temp := make([]byte, size)
	var i int
	for _, v := range arr {
		i += copy(temp[i:], v)
	}
	return temp
}
