package util

import (
	"crypto/md5"
	"fmt"
	"math/rand"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/scrypt"
)

const split = "$"
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

// MD5 sting
func MD5(s string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(s)))
}

// MD5Byte bytes
func MD5Byte(b []byte) string {
	return fmt.Sprintf("%x", md5.Sum(b))
}

// GenUUID in md5 hash
func GenUUID() string {
	u := uuid.NewV1()
	return MD5(u.String())
}

// RandString to get random length string code
func RandString(n int) string {
	b := make([]byte, n)
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}
	return string(b)
}

// EncryptWithSalt key with salt
// return [salt]$[hash]
func EncryptWithSalt(key string, salt string) string {
	hashKey, _ := scrypt.Key([]byte(key), []byte(salt), 16384, 8, 1, 32)
	s := make([]string, len(hashKey))
	for i := range hashKey {
		s[i] = string(hashKey[i])
	}
	sec := fmt.Sprintf("%s%s%s", salt, split, MD5(fmt.Sprintf("%s%s", salt, strings.Join(s, ""))))
	return sec
}

// Encrypt key with random salt
// return [salt]$[hash]
func Encrypt(key string) string {
	salt := RandString(6)
	return EncryptWithSalt(key, salt)
}

// Decrypt check hash string with salt
// return 0 equal, < less, > great
func Decrypt(raw string, hashPassword string) int {
	hash := strings.Split(hashPassword, split)
	salt := hash[0]
	return strings.Compare(EncryptWithSalt(raw, salt), hashPassword)
}
