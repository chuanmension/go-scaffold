package util

import (
	"errors"
	"fmt"
	"os"
	"reflect"
	"strings"

	"github.com/spf13/cast"
)

var ErrorCannotSet = errors.New("can not set")

func getEnv(key string) string {
	key = strings.ToUpper(key)
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return ""
}

// GetEnvString TL;DR
func GetEnvString(key string) string {
	return getEnv(key)
}

// GetEnvInt TL;DR
func GetEnvInt(key string) int {
	return cast.ToInt(getEnv(key))
}

func getFieldEnvName(f reflect.StructField) string {
	if t, ok := f.Tag.Lookup("json"); ok {
		return t
	}
	return f.Name
}

func getFieldSep(f reflect.StructField) string {
	if t, ok := f.Tag.Lookup("sep"); ok {
		return t
	}
	return ","
}

func setFieldValue(v reflect.Value, field reflect.StructField, structName string) error {
	f := v.FieldByName(field.Name)
	if !f.CanSet() {
		return ErrorCannotSet
	}
	key := fmt.Sprintf("%s_%s", structName, getFieldEnvName(field))
	value := strings.TrimSpace(GetEnvString(key))
	if value == "" {
		return nil
	}
	switch field.Type.Kind() {
	case reflect.String:
		f.SetString(value)
	case reflect.Int:
		f.SetInt(cast.ToInt64(value))
	case reflect.Bool:
		f.SetBool(cast.ToBool(value))
	case reflect.Float32, reflect.Float64:
		f.SetFloat(cast.ToFloat64(value))
	case reflect.Slice, reflect.Array:
		sep := getFieldSep(field)
		f.Set(reflect.ValueOf(strings.Split(value, sep)))
	default:
		return fmt.Errorf("unsupported type:%s", field.Type.String())
	}
	return nil
}

// InitStructFromEnv by reflect
// i interface{} = struct instance
// support Kind:
// String
// Int
// Bool
// Float32
// Float64
// Slice of string
func InitStructFromEnv(i interface{}) reflect.Value {
	t := reflect.TypeOf(i)
	// The application must call Elem() twice to get the struct value:
	// https://stackoverflow.com/questions/63421976/panic-reflect-call-of-reflect-value-fieldbyname-on-interface-value
	// get value ptr
	v := reflect.ValueOf(&i).Elem()
	tmp := reflect.New(v.Elem().Type()).Elem()
	tmp.Set(v.Elem())
	// get struct name
	name := t.Name()
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		err := setFieldValue(tmp, f, name)
		if err != nil {
			println(err.Error())
		}
	}
	//v.Set(tmp)
	return tmp
}

// PrintEnvKey show struct field from env key
func PrintEnvKey(i interface{}) {
	t := reflect.TypeOf(i)
	// get struct name
	name := t.Name()
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		key := fmt.Sprintf("%s_%s", name, getFieldEnvName(f))
		println(strings.ToUpper(key))
	}
}
