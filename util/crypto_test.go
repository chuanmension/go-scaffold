package util

import "testing"

const key = "hello"
const slat = "world"
const encrypt = "world$366a61706fa1badaefa736276e96cec9"

func TestEncrypt(t *testing.T) {
	s := Encrypt(key)
	t.Log(s)
}

func TestDecrypt(t *testing.T) {
	t.Log(Decrypt(key, encrypt) == 0)
}

func TestEncryptWithSalt(t *testing.T) {
	t.Log(EncryptWithSalt(key, slat) == encrypt)
}
