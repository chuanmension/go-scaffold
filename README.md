# Go scaffold

Use `go-scaffold` to build a `Go` gRPC project quickly.

## Getting Started

```shell
go get gitee.com/chuanmension/go-scaffold
```

## Overview

`go-scaffold` born for quickly build gRPC and HTTP services based on grpc-gateway

`go-scaffold` provides:

* gRPC/http base server configure
* Wrapper function for gRPC/http request dispatch
* Basic recover interceptor
* Scalable interceptor
* Optional zap logger
* gRPC client connector with dial option
* Cobra entry
* Database connection (Mysql and Redis)
* Init config struct from env
* Other commonly used tools

## Usage

See `example`
