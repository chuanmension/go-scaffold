package server

import (
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"net/http"
	"strings"

	"google.golang.org/grpc"
)

const keyContentType = "Content-Type"

const keyGrpcHeader = "application/grpc"

// GRPCHandlerFunc for request dispatch
func GRPCHandlerFunc(g *grpc.Server, h http.Handler) http.Handler {
	return h2c.NewHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.ProtoMajor == 2 && strings.EqualFold(r.Header.Get(keyContentType), keyGrpcHeader) || h == nil {
			g.ServeHTTP(w, r)
		} else {
			h.ServeHTTP(w, r)
		}
	}), &http2.Server{})
}
