/**
 * @Author ZoeAllen
 * @create 2020/8/18 8:44 下午
 */

package server

var defaultServerOptions = Options{
	CORSAllowedMethods: []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD"},
	CORSAllowedOrigins: []string{"*"},
	CORSAllowedHeaders: []string{"Origin", "Content-Length", "Content-Type", "Authorization", "X-Requested-With"},
	UseProtoNames:      true,
	EmitUnpopulated:    false,
	DiscardUnknown:     true,
	UseEnumNumbers:     false,
}

type Option func(options *Options)

// ConnectOptions for hub connection config
type Options struct {
	CORSAllowedMethods []string
	CORSAllowedOrigins []string
	CORSAllowedHeaders []string
	UseProtoNames      bool
	EmitUnpopulated    bool
	DiscardUnknown     bool
	UseEnumNumbers     bool
}
