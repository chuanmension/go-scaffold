package entry

import (
	"gitee.com/chuanmension/go-scaffold/conf"
	"gitee.com/chuanmension/go-scaffold/db"
	"gitee.com/chuanmension/go-scaffold/util"
	"log"
	"os"
	"sync"

	"go.uber.org/zap"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

var rootCmd *cobra.Command
var cmdOnce sync.Once

type CommandOptions struct {
	Use            string
	Short          string
	Long           string
	ConfigFilePath string
}

type CommandOption func(options *CommandOptions)

var defaultCommandOptions = CommandOptions{
	Use:            "app",
	Short:          "applications",
	Long:           "applications",
	ConfigFilePath: "./config.toml",
}

// NewRootCmd call once for global RootCmd initialization
func NewRootCmd(opts ...CommandOption) *cobra.Command {
	cmdOnce.Do(func() {
		options := defaultCommandOptions
		for _, fn := range opts {
			fn(&options)
		}
		// RootCmd represents the base command when called without any sub commands
		rootCmd = &cobra.Command{
			Use:   options.Use,
			Short: options.Short,
			Long:  options.Long,
		}
		rootCmd.PersistentFlags().StringVar(&cfgFile, "config", options.ConfigFilePath, "config file path")
	})
	return rootCmd
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if rootCmd == nil {
		NewRootCmd()
	}
	if err := rootCmd.Execute(); err != nil {
		println(err.Error())
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			println(err.Error())
			os.Exit(1)
		}

		// Search config in home directory with name (without extension).
		viper.SetConfigName("config")
		viper.AddConfigPath(home)
		// optionally look for config in the working directory
		viper.AddConfigPath(".")
	}

	// read in environment variables that match
	viper.AutomaticEnv()

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		zap.L().Debug("initConfig", zap.String("file", viper.ConfigFileUsed()), zap.String("error", err.Error()))
	}

	// unmarshal to config struct
	err := viper.Unmarshal(conf.Config)
	if err != nil {
		log.Fatalf("unable to decode into config struct, %v", err)
	}
	// init log
	util.InitRotateLog(conf.Config.EnableDebug, conf.Config.LogFile)
	// init db after config loaded
	db.InitClient()
	zap.L().Debug("initConfig success", zap.String("file", viper.ConfigFileUsed()))
}
