/**
 * @Author ZoeAllen
 * @create 2020/8/25 3:52 下午
 */

package main

import (
	"gitee.com/chuanmension/go-scaffold/http/middleware"
	"gitee.com/chuanmension/go-scaffold/http/server"
	"gitee.com/chuanmension/go-scaffold/util"
	"github.com/gin-gonic/gin"
	"net/http"
)

func indexHandler(c *gin.Context) {
	c.JSON(server.FormatResponse(http.StatusOK, "hi"))
}

func main() {
	util.InitLog(true, "")
	s := server.BaseServer{}
	r := s.NewEngine(func(options *server.Options) {
		options.Name = "test"
		options.CORSAllowedHeaders = []string{"Origin", "Content-Length", "Content-Type", "Authorization", "X-Requested-With", "Grpc-Metadata-Client", "Grpc-Metadata-Version"}
	})
	r.GET("", middleware.LoggerMiddleware(), indexHandler)
	s.Start()
}
