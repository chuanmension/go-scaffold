/**
 * @Author ZoeAllen
 * @create 2020/8/19 5:41 下午
 */

package main

import (
	"context"
	"gitee.com/chuanmension/go-scaffold/example/grpc/hello"
	"gitee.com/chuanmension/go-scaffold/grpc/client"
	"google.golang.org/grpc"
	"testing"
)

func TestServer(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "127.0.0.1:8000", grpc.WithInsecure())
	if err != nil {
		t.Fatalf("grpc.DialContext failed: %v", err)
	}
	defer conn.Close()

	cli := hello.NewHelloClient(conn)
	req := &hello.SayHiRequest{
		Name: "test",
	}
	resp, err := cli.SayHi(context.Background(), req)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(resp.Text)
}

func TestClient(t *testing.T) {
	connector := &client.Connector{
		Host:     "127.0.0.1",
		Port:     8000,
		Deadline: 5,
	}
	opts := []grpc.DialOption{
		grpc.WithPerRPCCredentials(CustomAuth{
			Client:                  "test",
			Version:                 "v0.1.0",
			ApiKey:                  "key",
			ApiSecret:               "secret",
			EnableTransportSecurity: false,
		}),
	}
	conn, err := connector.Connect(opts...)
	if err != nil {
		t.Fatal(err.Error())
	}
	cli := hello.NewHelloClient(conn)
	req := &hello.SayHiRequest{
		Name: "test",
	}
	resp, err := cli.SayHi(context.Background(), req)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(resp.Text)
}
