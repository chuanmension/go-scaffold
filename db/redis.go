package db

import (
	"gitee.com/chuanmension/go-scaffold/conf"
	"github.com/gomodule/redigo/redis"
	"sync"
	"time"

	"go.uber.org/zap"
)

var redisOnce sync.Once
var redisInstance *redis.Pool

// GetRedisClient of redis pool
func GetRedisClient(c conf.RedisConfig) *redis.Pool {
	redisOnce.Do(func() {
		redisInstance = &redis.Pool{
			MaxIdle:   c.MaxIdle,
			MaxActive: c.MaxActive,
			Dial: func() (redis.Conn, error) {
				c, err := redis.Dial("tcp", c.Host+":"+c.Port,
					redis.DialPassword(c.Password),
					redis.DialDatabase(c.Db),
					redis.DialConnectTimeout(time.Duration(c.ConnectionTimeout)*time.Millisecond),
					redis.DialReadTimeout(time.Duration(c.ReadTimeout)*time.Millisecond),
					redis.DialWriteTimeout(time.Duration(c.WriteTimeout)*time.Millisecond))
				if err != nil {
					zap.L().Error(err.Error())
					return nil, err
				}
				return c, nil
			},
			// Use the TestOnBorrow function to check the health of an idle connection
			// before the connection is returned to the application.
			TestOnBorrow: func(c redis.Conn, t time.Time) error {
				if time.Since(t) < time.Minute {
					return nil
				}
				_, err := c.Do("PING")
				return err
			},
			IdleTimeout: time.Duration(c.IdleTimeout) * time.Second,
			// If Wait is true and the pool is at the MaxActive limit,
			// then Get() waits for a connection to be returned to the pool before returning
			Wait: true,
		}
		zap.L().Info("init redis", zap.String("host", c.Host), zap.String("port", c.Port))
	})
	return redisInstance
}
