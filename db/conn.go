package db

import (
	"gitee.com/chuanmension/go-scaffold/conf"
	"github.com/gomodule/redigo/redis"
	"xorm.io/xorm"
)

// Redis client in global
var Redis *redis.Pool

// MySQL client in global
var MySQL *xorm.Engine

// InitClient db connections
// call this func after conf.Config loaded
// after InitClient success, db.Redis / db.MySQL can use in global context
func InitClient() {
	// init redis
	if conf.Config.EnableRedis {
		Redis = GetRedisClient(conf.Config.RedisConfig)
	}
	// init mysql
	if conf.Config.EnableMySQL {
		MySQL = GetMySQLClient(conf.Config.MySQLConfig)
	}
}
