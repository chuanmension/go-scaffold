package db

import (
	"fmt"
	"gitee.com/chuanmension/go-scaffold/conf"
	"log"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"go.uber.org/zap"
	"xorm.io/xorm"
)

// StatusInitialize for data status init
const StatusInitialize = 0

// StatusDelete for data status delete
const StatusDelete = 0

// StatusActive for data status active
const StatusActive = 1

// StatusDisable for data status disabled
const StatusDisable = 4

// MaxPage limit
const MaxPage = 100

// MaxPageSize limit
const MaxPageSize = 1000

// DefaultPage value
const DefaultPage = 1

// DefaultPageSize value
const DefaultPageSize = 10

// BasicDateFormat for time format layout
const BasicDateFormat = "2006-01-02"

// BasicDatetimeFormat for time format layout
const BasicDatetimeFormat = "2006-01-02 15:04:05"

const driverName = "mysql"

var mysqlOnce sync.Once

var mysqlEngine *xorm.Engine

// GetMySQLClient init connection once
func GetMySQLClient(c conf.MySQLConfig) *xorm.Engine {
	mysqlOnce.Do(func() {
		var err error
		// set connection string
		var dataSourceName string
		dataSourceName = fmt.Sprintf("tcp(%s:%s)/%s", c.Host, c.Port, c.Name)
		if c.Charset == "" {
			c.Charset = "utf8"
		}
		if c.Timeout == 0 {
			c.Timeout = 30
		}
		dataSourceName = fmt.Sprintf("%s:%s@%s?charset=%s&timeout=%ds&parseTime=true", c.Username, c.Password, dataSourceName, c.Charset, c.Timeout)
		// new mysql engine
		mysqlEngine, err = xorm.NewEngine(driverName, dataSourceName)
		if err != nil {
			log.Fatalf(err.Error())
		}
		err = mysqlEngine.Ping()
		if err != nil {
			log.Fatalf(err.Error())
		}
		mysqlEngine.SetMaxOpenConns(c.MaxOpenConns)
		mysqlEngine.SetMaxIdleConns(c.MaxIdleConns)
		mysqlEngine.SetConnMaxLifetime(time.Duration(c.ConnMaxLifetime))
		zap.L().Info("init mysql", zap.String("host", c.Host), zap.String("port", c.Port))
	})
	return mysqlEngine
}

// ExecuteSQL TL;DR
// return affected count
func ExecuteSQL(sqlOrArgs ...interface{}) (int64, error) {
	result, err := mysqlEngine.Exec(sqlOrArgs...)
	if err == nil {
		// get affected count
		return result.RowsAffected()
	}
	return 0, err
}

func Insert(sqlOrArgs ...interface{}) (int64, error) {
	result, err := mysqlEngine.Exec(sqlOrArgs...)
	if err == nil {
		// get affected count
		return result.LastInsertId()
	}
	return 0, err
}

func Update(sqlOrArgs ...interface{}) (int64, error) {
	return ExecuteSQL(sqlOrArgs...)
}

func Delete(sqlOrArgs ...interface{}) (int64, error) {
	return ExecuteSQL(sqlOrArgs...)
}
