package middleware

import (
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// LoggerMiddleware for base request log
func LoggerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		// before request
		c.Next()
		// after request
		// access the status we are sending
		status := c.Writer.Status()
		zap.L().Info("req",
			zap.String("url", c.Request.RequestURI),
			zap.Duration("cost", time.Since(start)),
			zap.Int("status", status),
			zap.String("method", c.Request.Method),
		)
	}
}
