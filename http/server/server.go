package server

import (
	"context"
	"fmt"
	"gitee.com/chuanmension/go-scaffold/conf"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/pprof"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"go.uber.org/zap"

	"github.com/gin-gonic/gin"
)

// BaseServer for gRPC
type BaseServer struct {
	listenAddr string
	httpEngine *gin.Engine
	option     Options
}

func (s *BaseServer) NewEngine(opts ...Option) *gin.Engine {
	// with options
	options := defaultServerOptions
	for _, fn := range opts {
		fn(&options)
	}
	s.option = options
	// set release run mode
	if !conf.Config.EnableDebug {
		gin.SetMode(gin.ReleaseMode)
		gin.DisableConsoleColor()
	} else {
		gin.SetMode(gin.DebugMode)
	}
	r := gin.New()
	if conf.Config.EnableDebug {
		pprof.Register(r)
	}
	if !s.option.DisableCORS {
		// use cors
		r.Use(cors.New(cors.Config{
			AllowAllOrigins:  true,
			AllowMethods:     s.option.CORSAllowedMethods,
			AllowHeaders:     s.option.CORSAllowedHeaders,
			AllowCredentials: false,
			MaxAge:           24 * time.Hour,
		}))
	}
	// use recovery middleware
	r.Use(gin.Recovery())
	s.httpEngine = r
	return r
}

// Start http server
func (s *BaseServer) Start() {
	zap.L().Debug("http server start",
		zap.String("host", s.option.Host),
		zap.Int("port", s.option.Port),
		zap.String("name", s.option.Name),
	)
	// start
	s.listenAddr = fmt.Sprintf("%s:%d", s.option.Host, s.option.Port)
	server := &http.Server{
		Addr:              s.listenAddr,
		Handler:           s.httpEngine,
		ReadTimeout:       s.option.ReadTimeout,
		ReadHeaderTimeout: s.option.ReadHeaderTimeout,
		WriteTimeout:      s.option.WriteTimeout,
		MaxHeaderBytes:    s.option.MaxHeaderBytes,
	}
	go func() {
		if s.option.SSLCrtFile != "" && s.option.SSLKeyFile != "" {
			if err := server.ListenAndServeTLS(s.option.SSLCrtFile, s.option.SSLKeyFile); err != nil {
				zap.L().Fatal("server error", zap.String("msg", err.Error()))
			}
		} else {
			if err := server.ListenAndServe(); err != nil {
				zap.L().Fatal("server error", zap.String("msg", err.Error()))
			}
		}
	}()
	// graceful shutdown
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	zap.L().Info("server graceful shutdown")
	// wait for 5 second
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		zap.L().Fatal("shutdown error", zap.String("msg", err.Error()))
	}
	zap.L().Info("server exit")
}
